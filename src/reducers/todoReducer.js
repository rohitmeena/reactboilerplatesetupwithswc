import types from '../actions/types';

const initialState = {
    list : []
}


export const listReducer = (state=initialState, action) => {
    let newList;

    switch(action.type){

        case types.ADD_ITEM : 
            return {...state, list : [...state.list, action.payload] }

        case types.UPDATE_ITEM : 
            newList = [...state.list];
            const itemIndexAtId = newList.findIndex((item) => item.id === action.payload.id);
            if (itemIndexAtId > -1) {
              newList[itemIndexAtId] = action.payload;
            }
            return {...state, list : newList } 

        case  types.DELETE_ITEM :
            newList = [...state.list];
            newList = newList.filter((item) => item.id !== action.payload);  
            return {...state, list : newList } 

        case types.CLEAR_ITEM:
            return {...state, list : []} 

        default:
            return state; 
               
    }
}